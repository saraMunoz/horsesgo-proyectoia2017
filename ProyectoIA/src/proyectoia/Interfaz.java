/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoia;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author sara
 */
public class Interfaz extends javax.swing.JFrame {

    /**
     * Creates new form Interfaz
     */
    
    Manejadora maneja;
    
    public Interfaz() {
        initComponents();
       
        maneja= new Manejadora();
        
        
        jcb_modojuego.addItemListener(maneja);
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnl_izq = new javax.swing.JPanel();
        jcb_modojuego = new javax.swing.JComboBox<>();
        lbl_instruccion = new javax.swing.JLabel();
        lbl_titulo = new javax.swing.JLabel();
        lbl_maquinaIcono = new javax.swing.JLabel();
        lbl_jugadorIcono = new javax.swing.JLabel();
        lbl_jug = new javax.swing.JLabel();
        lbl_maq = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        panelCentral = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1180, 670));
        setResizable(false);

        pnl_izq.setBackground(java.awt.Color.black);
        pnl_izq.setPreferredSize(new java.awt.Dimension(400, 464));
        pnl_izq.setLayout(null);

        jcb_modojuego.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        jcb_modojuego.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione", "Principiante", "Intermedio", "Avanzado" }));
        pnl_izq.add(jcb_modojuego);
        jcb_modojuego.setBounds(80, 250, 238, 29);

        lbl_instruccion.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        lbl_instruccion.setForeground(java.awt.Color.white);
        lbl_instruccion.setText("Seleccione el modo de juego");
        pnl_izq.add(lbl_instruccion);
        lbl_instruccion.setBounds(80, 210, 238, 28);

        lbl_titulo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/proyectoia/img/titulo.png"))); // NOI18N
        pnl_izq.add(lbl_titulo);
        lbl_titulo.setBounds(0, 10, 380, 201);

        lbl_maquinaIcono.setIcon(new javax.swing.ImageIcon(getClass().getResource("/proyectoia/img/cab1label.png"))); // NOI18N
        pnl_izq.add(lbl_maquinaIcono);
        lbl_maquinaIcono.setBounds(130, 430, 40, 40);

        lbl_jugadorIcono.setIcon(new javax.swing.ImageIcon(getClass().getResource("/proyectoia/img/cab22label.png"))); // NOI18N
        pnl_izq.add(lbl_jugadorIcono);
        lbl_jugadorIcono.setBounds(130, 500, 40, 40);

        lbl_jug.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        lbl_jug.setForeground(java.awt.Color.white);
        lbl_jug.setText("Jugador");
        pnl_izq.add(lbl_jug);
        lbl_jug.setBounds(190, 500, 90, 30);

        lbl_maq.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        lbl_maq.setForeground(java.awt.Color.white);
        lbl_maq.setText("Máquina");
        pnl_izq.add(lbl_maq);
        lbl_maq.setBounds(190, 430, 90, 30);

        jLabel3.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        jLabel3.setForeground(java.awt.Color.white);
        jLabel3.setText("Jugadores");
        pnl_izq.add(jLabel3);
        jLabel3.setBounds(80, 370, 180, 30);

        getContentPane().add(pnl_izq, java.awt.BorderLayout.WEST);

        panelCentral.setBackground(java.awt.Color.black);
        panelCentral.setLayout(new java.awt.CardLayout());
        getContentPane().add(panelCentral, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Interfaz().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel3;
    private javax.swing.JComboBox<String> jcb_modojuego;
    private javax.swing.JLabel lbl_instruccion;
    private javax.swing.JLabel lbl_jug;
    private javax.swing.JLabel lbl_jugadorIcono;
    private javax.swing.JLabel lbl_maq;
    private javax.swing.JLabel lbl_maquinaIcono;
    private javax.swing.JLabel lbl_titulo;
    private javax.swing.JPanel panelCentral;
    private javax.swing.JPanel pnl_izq;
    // End of variables declaration//GEN-END:variables

  
    
    
    public class Manejadora implements ItemListener {

        
        
        @Override
        public void itemStateChanged(ItemEvent e) {
           if (e.getSource() == jcb_modojuego) {
                int modo = jcb_modojuego.getSelectedIndex();
                if (modo == 1) {
                    PanelPrincipiante principiante = new PanelPrincipiante();
                    panelCentral.add(principiante, "1");
                    CardLayout distribuidor = (CardLayout) panelCentral.getLayout();
                    distribuidor.show(panelCentral, "1");
                } else if (modo == 2) {
                    PanelIntermedio intermedio = new PanelIntermedio();
                    panelCentral.add(intermedio, "2");
                    CardLayout distribuidor = (CardLayout) panelCentral.getLayout();
                    distribuidor.show(panelCentral, "2");
                } else if (modo == 3) {
                     PanelAvanzado avanzado = new PanelAvanzado();
                    panelCentral.add(avanzado, "3");
                    CardLayout distribuidor = (CardLayout) panelCentral.getLayout();
                    distribuidor.show(panelCentral, "3");
                }
            }
        }

        

       

    }

}
