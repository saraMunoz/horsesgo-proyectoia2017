/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoia;

import com.sun.webkit.Utilities;
import static java.lang.Math.abs;
import java.util.ArrayList;
import java.util.Stack;
import javax.swing.JButton;
import static java.lang.Math.abs;
import static java.lang.Math.abs;
import static java.lang.Math.abs;
import static java.lang.Math.abs;
import static java.lang.Math.abs;
import static java.lang.Math.abs;
import static java.lang.Math.abs;

/**
 *
 * @author invitado
 */
public class AlgoritmoMinMax {

    String posicion_solucion = "";
    Nodo nodoRaiz = null;
    ArrayList<Nodo> nodos_utilidades = null;
    int posfila_oponente;
    int poscol_oponente;
    Stack<Nodo> pila;
    int[][] matriz;
    //int[][] posiciones_validas;
//    ArrayList<ArrayList> posiciones_validas;
    int limite_arbol;
    ArrayList<Nodo> arbol;
    boolean gano_maquina;
    boolean gano_oponente;
    int limite_col = 0;
    int posfila_actual = 0;
    int poscol_actual = 0;

    public AlgoritmoMinMax(int[][] arreglo, int limite) {
        matriz = arreglo;
        limite_arbol = limite;
        pila = new Stack<Nodo>();
        arbol = new ArrayList<Nodo>();
        nodos_utilidades = new ArrayList<Nodo>();
        limite_col = limite_arbol - 1;
        gano_maquina = false;
        gano_oponente = false;
//        posiciones_validas = new ArrayList<ArrayList>();
        // posiciones_validas = new int[8][2];

    }

    /**
     * nodoRaizProblema Método que inicializa el nodo raíz de la busqueda, es
     * decir la posicion inicial del robot
     */
    void nodoRaizProblema() {

        for (int i = 0; i < limite_arbol; i++) {
            for (int j = 0; j < limite_arbol; j++) {
                if (matriz[i][j] == 2) {
                    //SE DESHABILITA LA POSICIONES DE LA RAIZ
                    matriz[i][j] = -1;
                    posfila_actual = i;
                    poscol_actual = j;
                    nodoRaiz = new Nodo(null, i, j, 0, -1000000, "MAX",
                            posicionesValidas(i, j).size(), posicionesValidas(posfila_actual, poscol_actual).size());
                    
                    //Se añade el nodo a la cola
                    pila.push(nodoRaiz);
                }
            }
        }
    }

    public void sacarOponente() {

        for (int i = 0; i < limite_arbol; i++) {
            for (int j = 0; j < limite_arbol; j++) {
                if (matriz[i][j] == 3) {

                    posfila_oponente = i;
                    poscol_oponente = j;

                }
            }

        }
    }

    boolean MinMax() {
        //Se incializa el nodo raiz
        sacarOponente();
        nodoRaizProblema();
      //  System.out.println("matriz " + matriz[0][0]);
//        System.out.println("ndo raiz:"+nodoRaiz.getfilaActual()+" "+nodoRaiz.getcolumnaActual());

        System.out.println("oponente :" + posfila_oponente + " " + poscol_oponente);

        while (!pila.isEmpty()) {

            if (pila.isEmpty()) {
                return false;
            } else {
                Nodo nodoActual = pila.pop();
                arbol.add(nodoActual);
                if (nodoActual.getProfundidad() == limite_arbol) {

                    posicion_solucion = retornarSolucion();
                    return true;

                }else
                if(posicionesValidas(nodoActual.getfilaActual(), nodoActual.getcolumnaActual()).size() == 0){
                
                    posicion_solucion = retornarSolucionHoja(nodoActual);
                    return true;

                
                }else 
                {

                    System.out.println("entre a expandir: " + nodoActual.getfilaActual() + " , " + nodoActual.getcolumnaActual());
                    arbol.add(nodoActual);
                    expandir(nodoActual);
                }
             //   System.out.println("hdkjhkjwe");
                //Si es la primera vez que expando, es decir si en el arreglo del historial solo se tiene al nodo raíz
            }
        }

        return false;
    }

    public ArrayList<ArrayList> posicionesValidas(int i, int j) {

        ArrayList<ArrayList> posiciones_validas = new ArrayList<ArrayList>();

        
        
        if (i < 2 || j == limite_col || matriz[i - 2][j + 1] == -1 || matriz[i-2][j+1] == 3) {
        } else  {System.out.println("puedo ir  a: "+(i - 2)+" , "+(j + 1));
            ArrayList posiciones = new ArrayList();
            posiciones.add(i - 2);
            posiciones.add(j + 1);
            posiciones_validas.add(posiciones);
        }

        if (i < 2 || j == 0 || matriz[i - 2][j - 1] == -1 || matriz[i-2][j-1] == 3) {
        } else {System.out.println("puedo ir  a: "+(i - 2)+" , "+(j - 1));
            ArrayList posiciones = new ArrayList();
            posiciones.add(i - 2);
            posiciones.add(j - 1);
            posiciones_validas.add(posiciones);
        }

        if (i >= limite_col - 1 || j == limite_col  || matriz[i + 2][j + 1] == -1 || matriz[i+2][j+1] == 3) {
        } else {System.out.println("puedo ir  a: "+(i + 2)+" , "+(j + 1));
            ArrayList posiciones = new ArrayList();
            posiciones.add(i + 2);
            posiciones.add(j + 1);
            posiciones_validas.add(posiciones);
        }

        if (i >= limite_col - 1 || j == 0  || matriz[i + 2][j - 1] == -1 || matriz[i+2][j-1] == 3) {
        } else {System.out.println("puedo ir  a: "+(i + 2)+" , "+(j - 1));
            ArrayList posiciones = new ArrayList();
            posiciones.add(i + 2);
            posiciones.add(j - 1);
            posiciones_validas.add(posiciones);
        }

        if (i == 0 || j < 2  || matriz[i -1][j - 2] == -1 || matriz[i-1][j-2] == 3) {
        } else {System.out.println("puedo ir  a: "+(i - 1)+" , "+(j - 2));
            ArrayList posiciones = new ArrayList();
            posiciones.add(i - 1);
            posiciones.add(j - 2);
            posiciones_validas.add(posiciones);
        }

        if (i == limite_col || j < 2  || matriz[i +1][j - 2] == -1 || matriz[i+1][j-2] == 3) {
        } else {System.out.println("puedo ir  a: "+(i + 1)+" , "+(j - 2));
            ArrayList posiciones = new ArrayList();
            posiciones.add(i + 1);
            posiciones.add(j - 2);
            posiciones_validas.add(posiciones);
        }

        if (i == 0 || j >= limite_col - 1  || matriz[i - 1][j +2] == -1 || matriz[i-1][j+2] == 3) {
        } else {System.out.println("puedo ir  a: "+(i - 1)+" , "+(j + 2));
            ArrayList posiciones = new ArrayList();
            posiciones.add(i - 1);
            posiciones.add(j + 2);
            posiciones_validas.add(posiciones);
        }

        if (i == limite_col || j >= limite_col - 1  || matriz[i +1][j +2] == -1 || matriz[i+1][j+2] == 3) {
        } else {System.out.println("puedo ir  a: "+(i + 1)+" , "+(j + 2));
            ArrayList posiciones = new ArrayList();
            posiciones.add(i + 1);
            posiciones.add(j + 2);
            posiciones_validas.add(posiciones);
        }

        return posiciones_validas;
    }

    public String getMensajeRuta() {
        return posicion_solucion;
    }

    private void expandir(Nodo nodoActual) { sacarOponente();
        int posfila = nodoActual.getfilaActual();
        int poscol = nodoActual.getcolumnaActual();
        String tipoNodoActual = nodoActual.getTipo();

        ArrayList<ArrayList> posiciones_validas = posicionesValidas(posfila, poscol);

       //  System.out.println("posición nodo: "+posfila+","+poscol+"**********disponibles: "+posicionesValidas(posfila, poscol).size());
       
      
       
        System.out.println("en el nodo de posicion: "+posfila+","+poscol+" tengo diponibles: "+posiciones_validas.size());
        
        for (int k = 0; k < posiciones_validas.size(); k++) {

            int posfila_valida = (int) posiciones_validas.get(k).get(0);
            int poscol_valida = (int) posiciones_validas.get(k).get(1);
            //Se verifica que si la posiicon es válida         

                int disponiblesMaquina = posicionesValidas(posfila_valida, poscol_valida).size();
                
                int disponiblesOponente = posicionesValidas(posfila_oponente, poscol_oponente).size();

                
                
                Nodo nodoHijo = null;
                if (tipoNodoActual.equalsIgnoreCase("MAX")) {
                    nodoHijo = new Nodo(nodoActual, posfila_valida, poscol_valida, nodoActual.getProfundidad() + 1, 1000000, "MIN", disponiblesOponente, disponiblesMaquina);

                } else {
                    nodoHijo = new Nodo(nodoActual, posfila_valida, poscol_valida, nodoActual.getProfundidad() + 1, -1000000, "MAX", disponiblesOponente, disponiblesMaquina);

                }
                nodoActual.setEsHoja(false);
               arbol.add(nodoHijo);
                pila.push(nodoHijo);

            
        }
    }

    private void setUtilidadNodosHoja() {

        for (int i = 0; i < arbol.size(); i++) {

            nodos_utilidades.add(arbol.get(i));
        }
       // System.out.println("tamaño arreglo:" + nodos_utilidades.size());
        for (int i = 0; i < nodos_utilidades.size(); i++) {

            if (nodos_utilidades.get(i).isEsHoja()) {
           //     System.out.println("soy hoja ");
            }

            if (nodos_utilidades.get(i).isEsHoja() && (nodos_utilidades.get(i).getDisponiblesMaquina() == 0)) {
                nodos_utilidades.get(i).setUtilidad(-1000000);
            } else if (nodos_utilidades.get(i).isEsHoja() && (nodos_utilidades.get(i).getDisponiblesOponente() == 0)) {
                nodos_utilidades.get(i).setUtilidad(1000000);
            } else {

                //System.out.println("ultilidad: " + abs(nodos_utilidades.get(i).getDisponiblesOponente() - nodos_utilidades.get(i).getDisponiblesMaquina()));
                nodos_utilidades.get(i).setUtilidad(nodos_utilidades.get(i).getDisponiblesMaquina() - nodos_utilidades.get(i).getDisponiblesOponente());
            }
        }
    }

    private Nodo nodoMasProfundo() {
        Nodo mayor_profundidad = nodos_utilidades.get(0);
        for (int i = 1; i < nodos_utilidades.size(); i++) {

            if (mayor_profundidad.getProfundidad() < nodos_utilidades.get(i).getProfundidad()) {

                mayor_profundidad = nodos_utilidades.get(i);

            }

        }
       // System.out.println("Sali del for nodo mas profundo " + mayor_profundidad.getProfundidad());
        return mayor_profundidad;

    }

    private void subirUtilidades() {

        while (nodos_utilidades.size() > 1) {
            Nodo mayor_profundidad = nodoMasProfundo();
            if (mayor_profundidad.getProfundidad() == 0) {
         //       System.out.println("soy la raiz " + mayor_profundidad.getcolumnaActual() + " " + mayor_profundidad.getfilaActual());
                break;
            } else {
                Nodo nodo_padre = mayor_profundidad.getPadre();
                if (nodo_padre.getTipo().equalsIgnoreCase("MAX")) {
                    if (mayor_profundidad.getUtilidad() < nodo_padre.getUtilidad()) {

                    } else {

                        mayor_profundidad.getPadre().setUtilidad(mayor_profundidad.getUtilidad());
                    }

                } else if (mayor_profundidad.getUtilidad() < nodo_padre.getUtilidad()) {

                    mayor_profundidad.getPadre().setUtilidad(mayor_profundidad.getUtilidad());
                } else {
                }

                nodos_utilidades.remove(mayor_profundidad);

            }
        }
    }
    
    
    private String retornarSolucionHoja(Nodo hoja){
        String solucion = "";
        Nodo nodo_solucion = hoja;
        if(matriz[nodo_solucion.getfilaActual()][nodo_solucion.getcolumnaActual()] == 3 ||matriz[nodo_solucion.getfilaActual()][nodo_solucion.getcolumnaActual()] == -1 ){
        
            
    solucion = "(" + nodo_solucion.getfilaActual()+ "," + nodo_solucion.getcolumnaActual() + ")";
        }else{
        
        solucion = "(" + nodo_solucion.getfilaActual() + "," + nodo_solucion.getcolumnaActual() + ")";
        matriz[nodo_solucion.getfilaActual()][nodo_solucion.getcolumnaActual()] = -1;
        
        
        }
    
//    String solucion = "";
    
        
        System.out.println("posición oponente: "+posfila_oponente+","+poscol_oponente+"**********disponibles oponente: "+posicionesValidas(posfila_oponente, poscol_oponente).size());
        System.out.println("posicion solucion: "+nodo_solucion.getfilaActual()+","+nodo_solucion.getcolumnaActual()+"**********disponibles solucion: "+posicionesValidas(nodo_solucion.getfilaActual(), nodo_solucion.getcolumnaActual()).size());
       
 
       sacarOponente();
        if (posicionesValidas(posfila_oponente, poscol_oponente).size() == 0) {

            gano_maquina = true;

        } else if (posicionesValidas(nodo_solucion.getfilaActual(), nodo_solucion.getcolumnaActual()).size() == 0) {

            gano_oponente = true;

        }
        return solucion;
    
    
    }

    private String retornarSolucion() {
        setUtilidadNodosHoja();
        subirUtilidades();

        ArrayList<Nodo> nodos_prof1 = new ArrayList<Nodo>();
        String solucion = "";
        for (int i = 0; i < arbol.size(); i++) {
            if (arbol.get(i).getProfundidad() == 1) {

                nodos_prof1.add(arbol.get(i));
            }
        }
        Nodo nodo_solucion = nodos_prof1.get(0);
        for (int i = 1; i < nodos_prof1.size(); i++) {

            if (nodos_prof1.get(i).getUtilidad() >= nodo_solucion.getUtilidad()) {

                nodo_solucion = nodos_prof1.get(i);

            }
        }
        solucion = "(" + nodo_solucion.getfilaActual() + "," + nodo_solucion.getcolumnaActual() + ")";
        matriz[nodo_solucion.getfilaActual()][nodo_solucion.getcolumnaActual()] = -1;
        sacarOponente();
        
        System.out.println("posición oponente: "+posfila_oponente+","+poscol_oponente+"**********disponibles oponente: "+posicionesValidas(posfila_oponente, poscol_oponente).size());
        System.out.println("posicion solucion: "+nodo_solucion.getfilaActual()+","+nodo_solucion.getcolumnaActual()+"**********disponibles solucion: "+posicionesValidas(nodo_solucion.getfilaActual(), nodo_solucion.getcolumnaActual()).size());
       
 
       
        if (posicionesValidas(posfila_oponente, poscol_oponente).size() == 0) {

            gano_maquina = true;

        } else if (posicionesValidas(nodo_solucion.getfilaActual(), nodo_solucion.getcolumnaActual()).size() == 0) {

            gano_oponente = true;

        }
        return solucion;

    }

    public boolean isGano_maquina() {
        return gano_maquina;
    }

    public boolean isGano_oponente() {
        return gano_oponente;
    }

}
