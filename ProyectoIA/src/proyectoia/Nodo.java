/*
 * Universidad del Valle
 * Escuela de Ingenieria de Sistemas y Computación
 * Introducción a la inteligencia artificial
 * Primer Proyecto - Robot Sapiens
 *
 * Integrantes:
 * Jose Alejandro Libreros  Montaño      1427406 
 * Annie Paola Muñoz Llanos              1424440
 * Sara Catalina Muñoz Huertas           1422691
 */
package proyectoia;

//Se importan librerias
import java.util.ArrayList;

//Clase nodo
public class Nodo {

    //Se declaran variables globales
    Nodo padre;
    int profundidad;
    int filaActual;
    int columnaActual;    
    int utilidad;
    String tipo;
    boolean esHoja;
    int disponiblesOponente;
    int disponiblesMaquina;

    /**
     * Nodo
     * @param padre
     * @param filaActualRobot
     * @param columnaActualRobot
     * @param operador
     * @param profundidad
     * @param costo
     * @param disparos
     * @param historial
     * @param utilidad
     * @param funcion 
     * Constructor de la clase
     */
    public Nodo(Nodo padre, int filaActualRobot, int columnaActualRobot,  int profundidad, int utilidad, String tipo_nodo, int disponiblesOponente, int disponiblesMaquina) {
        this.padre = padre;
        this.profundidad = profundidad;
        this.filaActual = filaActualRobot;
        this.columnaActual = columnaActualRobot;
        this.utilidad = utilidad;
        this.tipo = tipo_nodo;
        this.esHoja = true;
        this.disponiblesMaquina = disponiblesMaquina;
        this.disponiblesOponente = disponiblesOponente;
    }

    public int getDisponiblesOponente() {
        return disponiblesOponente;
    }

    public void setDisponiblesOponente(int disponiblesOponente) {
        this.disponiblesOponente = disponiblesOponente;
    }

    public int getDisponiblesMaquina() {
        return disponiblesMaquina;
    }

    public void setDisponiblesMaquina(int disponiblesMaquina) {
        this.disponiblesMaquina = disponiblesMaquina;
    }

    public boolean isEsHoja() {
        return esHoja;
    }

    public void setEsHoja(boolean hoja) {
        this.esHoja = hoja;
    }


      

    /**
     * getPadre
     * @return -Nodo
     * Método que retorna el padre de un nodo
     */
    public Nodo getPadre() {
        return padre;
    }

     public int getUtilidad() {
        return utilidad;
    }
     
     public void setUtilidad(int uti) {
        utilidad = uti;
    }
     
     public String getTipo() {
        return tipo;
    }
  
    /**
     * getfilaActualRobot
     * @return int
     * Método que retorna la fila actual del robot
     */
    public int getfilaActual() {
        return filaActual;
    }

    /**
     * getcolumnaActualRobot
     * @return int
     * Método que retorna la columna actual de un robot
     */
    public int getcolumnaActual() {
        return columnaActual;
    }

    /**
     * getProfundidad
     * @return int
     * Método que retorna la profundidad de un nodo
     */
    public int getProfundidad() {
        return profundidad;
    }


}
